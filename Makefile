.PHONY: prepare-for-release
.PHONY: smoke-tests

prepare-for-release: release
	rm src/pds_template/pds_template.ml
	./build/release/pds/pds.native

smoke-tests: release
	cd smoke-tests && env MAKE=$(MAKE) ./test

test: smoke-tests

include pds.mk
