module List = ListLabels

module File_set = Set.Make(String)
module Build_set = Set.Make(String)

let pds_mk = "pds.mk"
let build_makefile = "build_Makefile"
let test_makefile= "test_Makefile"
let ocamlrules_mk = "Ocamlrules.mk.in"
let src_dir = "src"
let build_dir = "build"
let tests_dir = "tests"

let printf = Printf.printf
let fprintf = Printf.fprintf
let sprintf = Printf.sprintf

module Cmdline = struct
  module C = Cmdliner

  let format =
    let doc = "Output a tab representation of the config file." in
    C.Arg.(value & flag & info ["f"; "format"] ~doc)
end

module Lookup = struct
  type t = { build_type : string
           ; selector : string option
           ; builds_conf : TomlTypes.table
           }

  let rec lookup c = function
    | [] ->
      None
    | x::xs ->
      begin match TomlLenses.get c x with
        | Some v -> Some v
        | None -> lookup c xs
      end

  let build_lookup use_global test t build_name k typ =
    let src_or_tests = if test then "tests" else "src" in
    let selector =
      (* The variables for release builds do not have a build type in their key. *)
      match t.selector with
        | Some selector when t.build_type = "release" ->
          TomlLenses.([ key src_or_tests |-- table
                        |-- key build_name |-- table
                        |-- key "selector" |-- table
                        |-- key selector |-- table
                        |-- key k |-- typ
                      ])
        | Some selector ->
          (* Search first with the specific build type, then the release selector *)
          TomlLenses.([ key src_or_tests |-- table
                        |-- key build_name |-- table
                        |-- key "selector" |-- table
                        |-- key selector |-- table
                        |-- key t.build_type |-- table
                        |-- key k |-- typ
                      ; key src_or_tests |-- table
                        |-- key build_name |-- table
                        |-- key "selector" |-- table
                        |-- key selector |-- table
                        |-- key k |-- typ
                      ])
        | None ->
          []
    in
    let global_build_type = if test then "test-" ^ t.build_type else t.build_type in
    let global =
      TomlLenses.([ key "global" |-- table
                    |-- key global_build_type |-- table
                    |-- key k |-- typ
                  ])
    in
    let default =
      match t.build_type with
        | "release" ->
          TomlLenses.([ key src_or_tests |-- table
                        |-- key build_name |-- table
                        |-- key k |-- typ
                      ] @ if use_global then global else [])
        | build_type ->
          List.flatten
            TomlLenses.([ [ key src_or_tests |-- table
                            |-- key build_name |-- table
                            |-- key build_type |-- table
                            |-- key k |-- typ
                          ]
                        ; if use_global then global else []
                        ; [ key src_or_tests |-- table
                            |-- key build_name |-- table
                            |-- key k |-- typ
                          ]
                      ])
    in
    List.flatten [selector; default]

  let strings ?(use_global = false) ?(test = false) t build_name k =
    lookup
      t.builds_conf
      (build_lookup use_global test t build_name k TomlLenses.(array |-- strings))

  let string ?(use_global = false) ?(test = false) t build_name k =
    lookup
      t.builds_conf
      (build_lookup use_global test t build_name k TomlLenses.string)

  let bool ?(use_global = false) ?(test = false) t build_name k =
    lookup
      t.builds_conf
      (build_lookup use_global test t build_name k TomlLenses.bool)
end

let value_opt ~default = function
  | Some v -> v
  | None -> default

let value_exn ~msg = function
  | Some v -> v
  | None -> failwith msg

let endswith suffix str =
  let suffix_len = String.length suffix in
  let str_len = String.length str in
  if str_len >= suffix_len then
    suffix = String.sub str (str_len - suffix_len) suffix_len
  else
    false

let prepend_dir d fs = List.map ~f:(Filename.concat d) fs

let readdir dir =
  Sys.readdir dir
    |> Array.to_list
    |> List.map ~f:(Filename.concat dir)

let path_concat = function
  | [] ->
    assert false
  | p::ps ->
    List.fold_left
      ~f:Filename.concat
      ~init:p
      ps

let mkdir_p = function
  | [] ->
    assert false
  | ds ->
    ignore
      (List.fold_left
         ~f:(fun acc d ->
             let acc = Filename.concat acc d in
             try
               Unix.mkdir acc 0o744;
               acc
             with
               | Unix.Unix_error(Unix.EEXIST, "mkdir", _) ->
                 acc)
         ~init:"."
         ds)

let load_builds_conf pds_conf =
  if Sys.file_exists pds_conf then
    Toml.Parser.(from_filename pds_conf |> unsafe)
  else
    failwith (sprintf "Build config file, %s, must exist" pds_conf)

let get_dirs d =
  Sys.readdir d
  |> Array.to_list
  |> List.filter ~f:(fun f -> Sys.is_directory (Filename.concat d f))

(*
 * Builds are directories in the src directory
 *)
let get_builds () = Build_set.of_list (get_dirs src_dir)

(*
 * Tests are directories in the tests dir
 *)
let get_tests () =
  if Sys.file_exists tests_dir then
    Build_set.of_list (get_dirs tests_dir)
  else
    Build_set.empty

(*
 * Project types can be third-party, which involves doing nothing wiht them
 * or ocaml which pds generates configs for.
 *)
let get_project_type builds_conf build =
  let project_type =
    TomlLenses.(key "src" |-- table
                   |-- key build |-- table
                   |-- key "project_type" |-- string)
  in
  match TomlLenses.get builds_conf project_type with
    | Some "third-party" ->
      `Third_party
    | Some "ocaml"
    | None ->
      `Ocaml
    | Some project_type ->
      failwith (sprintf "Unknown project type %s for build %s" project_type build)

(*
 * For ocaml projects, a build can be a library or an executable
 *)
let get_project_target_type builds_conf build =
  let build_type =
    TomlLenses.(key "src" |-- table
                   |-- key build |-- table
                   |-- key "type" |-- string)
  in
  match get_project_type builds_conf build with
    | `Ocaml -> begin
      match TomlLenses.get builds_conf build_type with
        | Some "exec" ->
          `Exec
        | Some "library" | None ->
          `Library
        | Some build_type ->
          failwith (sprintf "Unknown build type %s for build %s" build_type build)
    end
    | `Third_party ->
      failwith (sprintf "Third party project %s has no build type" build)

let string_of_project_target_type = function
  | `Exec -> "exec"
  | `Library -> "library"

let string_of_project_type = function
  | `Ocaml -> "ocaml"
  | `Third_party -> "third-party"

let string_of_deps = String.concat ","

let compute_selector builds_conf =
  let path = TomlLenses.(key "global" |-- table |-- key "selector" |-- array |-- strings) in
  match TomlLenses.(get builds_conf path) with
    | Some (cmd::args) ->
      begin match Process.read_stdout cmd (Array.of_list args) with
        | [] -> failwith "Selector produced empty output"
        | [selector; ""]
        | [selector] ->
          (* TODO Verify the selector has a valid name *)
          Some selector
        | _ -> failwith "Selector produced more than one line of output."
      end
    | Some [] ->
      failwith "Selector cannot be an empty list."
    | None ->
      None

(*
 * A "lib module" is one which has a .mli file.  Since any .mli file needs to
 * have a .ml file associated with it, we just take the .mli files and replace
 * the end with .ml.
 *)
let calculate_lib_modules mli_files =
  mli_files
  |> File_set.elements
  |> List.map ~f:(fun f -> Filename.chop_extension f ^ ".ml")

let calculate_non_lib_modules ml_files mli_files =
  let lib_modules_set = File_set.of_list (calculate_lib_modules mli_files) in
  File_set.elements (File_set.diff ml_files lib_modules_set)

let calculate_byte_target name = function
  | `Library -> name ^ ".cma"
  | `Exec -> name ^ ".byte"

let calculate_native_target name = function
  | `Library -> name ^ ".cmxa"
  | `Exec -> name ^ ".native"

let get_ocaml_targets builds_conf b =
  match get_project_type builds_conf b with
    | `Ocaml ->
      let build_type = get_project_target_type builds_conf b in
      [ calculate_byte_target b build_type
      ; calculate_native_target b build_type
      ]
    | `Third_party ->
      []

let calculate_external_targets build_base_dir builds_conf internal_deps =
  internal_deps
  |> Build_set.elements
  |> List.map ~f:(fun b -> (b, get_ocaml_targets builds_conf b))
  |> List.map
    ~f:(fun (b, targets) ->
        let path = Filename.concat build_base_dir b in
        List.map (Filename.concat path) targets)
  |> List.concat
  |> File_set.of_list

let get_should_build ?test lookup build =
  value_opt
    ~default:true
    (Lookup.bool ?test lookup build "build")

let get_build_deps lookup build =
  Build_set.of_list
    (value_opt
       ~default:[]
       (Lookup.strings lookup build "deps"))

let get_compile_build_deps lookup build =
  Build_set.of_list
    (value_opt
       ~default:[]
       (Lookup.strings lookup build "compile_deps"))

let get_meta_linkopts lookup build =
  value_opt
    ~default:""
    (Lookup.string ~use_global:true lookup build "meta_linkopts")

let get_test_deps lookup test =
  Build_set.of_list
    (value_opt
       ~default:[]
       (Lookup.strings ~test:true lookup test "deps"))

(* Takes the type of the build (release, debug, etc) and the project type (exec
   or library), and the entire build configuration, and finally the name of the
   build being processed.

   The output of this is a makefile in the appropriate build directory
   (build/<build_type>/<build>/Makefile). *)
let emit_ocaml_build lookup project_type build =
  (* This is the directory we will use to build the makefile *)
  let src_path = path_concat [src_dir; build] in
  (* This is the directory that will appear in the makefile *)
  let src_dir = path_concat [".."; ".."; ".."; src_dir; build] in
  let build_base_dir = Filename.concat build_dir lookup.Lookup.build_type in
  let build_output_dir = Filename.concat build_base_dir build in
  let src_files = Array.to_list (Sys.readdir src_path) in
  let ml_files = File_set.of_list (List.filter (endswith ".ml") src_files) in
  let mli_files = File_set.of_list (List.filter (endswith ".mli") src_files) in
  let deps = get_build_deps lookup build in
  let internal_deps = Build_set.inter deps (get_builds ()) in
  let external_targets = calculate_external_targets ".." lookup.Lookup.builds_conf internal_deps in
  let lib_modules = calculate_lib_modules mli_files in
  let non_lib_modules = calculate_non_lib_modules ml_files mli_files in
  let byte_target = calculate_byte_target build project_type in
  let native_target = calculate_native_target build project_type in
  let extra_compiler_opts =
    value_opt
      ~default:""
      (Lookup.string ~use_global:true lookup build "extra_compiler_opts")
  in
  let extra_ocamldep_opts =
    value_opt
      ~default:""
      (Lookup.string ~use_global:true lookup build "extra_ocamldep_opts")
  in
  let meta_linkopts = get_meta_linkopts lookup build in
  let oc = open_out (Filename.concat build_output_dir "Makefile") in
  output_string
    oc
    (String.concat
       "\n\n"
       (List.concat
          [ begin match non_lib_modules with | [] -> [] | _ -> [".NOTPARALLEL:"] end
          (* If there are any non_lib_modules then make the build serial.  This
             is because generating .cmi's will conflict if they are generated in
             parallel. *)
          ; [ "CAMLP4="
            ; "SRC_DIR=" ^ src_dir
            ; "PACKAGES=" ^ String.concat " " (Build_set.elements deps)
            ; "LIB_MODULES=" ^ String.concat " " lib_modules
            ; "NON_LIB_MODULES=" ^ String.concat " " non_lib_modules
            ; "EXTERNAL_DEPS=" ^ String.concat " \\\n\t" (File_set.elements external_targets)
            ; "BYTE_TARGET=" ^ byte_target
            ; "NATIVE_TARGET=" ^ native_target
            ; "EXTRA_COMPILER_OPTS=" ^ extra_compiler_opts
            ; "EXTRA_OCAMLDEP_OPTS=" ^ extra_ocamldep_opts
            ; "TARGET_TYPE=" ^ string_of_project_target_type
                (get_project_target_type lookup.Lookup.builds_conf build)
            ; "META_LINKOPTS=" ^ meta_linkopts
            ; value_exn ~msg:"Error finding makefile template" (Pds_template.read build_makefile)
            ]
          ]));
  output_char oc '\n';
  let install =
    value_exn
      ~msg:(Printf.sprintf "%s is missing an 'install' key, which is required" build)
      (Lookup.bool lookup build "install")
  in
  let install_output =
    match (get_project_target_type lookup.Lookup.builds_conf build, install) with
      | (`Library, true) ->
        ["all: META"; ""; "install: install_lib"; ""; "remove: remove_lib"]
      | (`Library, false) ->
        ["all: META"; ""; "install: all"; ""; "remove:"]
      | (`Exec, true) ->
        let install_cmd =
          value_exn
            ~msg:"Installing executables requires an install_cmd"
            (Lookup.string lookup build "install_cmd")
        in
        let remove_cmd =
          value_exn
            ~msg:"Installing executables requires a remove_cmd"
            (Lookup.string lookup build "remove_cmd")
        in
        ["install: all"; "\t" ^ install_cmd; ""; "remove:"; "\t" ^ remove_cmd]
      | (`Exec, false) ->
        ["install: all"; ""; "remove:"]
  in
  output_string oc (String.concat "\n" install_output);
  let extra_makefile_lines =
    value_opt
      ~default:[]
      (Lookup.strings lookup build "extra_makefile_lines")
  in
  output_string
    oc
    (String.concat
       "\n\n"
       ([""] @
        extra_makefile_lines @
        [ "include ../../../Ocamlrules.mk.in"
        ; "$(native_cmx) $(byte_cmo) $(neutral_cmi): ../../../pds.conf"
        ; "-include .d"]));
  output_char oc '\n';
  close_out oc

let emit_third_party_build lookup build =
  let src_dir = path_concat [ ".."; ".."; ".."; src_dir; build ] in
  let oc = open_out (path_concat ["build"; lookup.Lookup.build_type; build; "Makefile"]) in
  output_string
    oc
    (String.concat
       "\n\n"
       [ "SRC_DIR=" ^ src_dir
       ; "include $(SRC_DIR)/Makefile"
       ]);
  close_out oc

let emit_build lookup build =
  match get_project_type lookup.Lookup.builds_conf build with
    | `Third_party ->
      mkdir_p ["build"; lookup.Lookup.build_type; build];
      emit_third_party_build lookup build
    | `Ocaml ->
      mkdir_p ["build"; lookup.Lookup.build_type; build];
      emit_ocaml_build lookup (get_project_target_type lookup.Lookup.builds_conf build) build

let emit_builds lookup =
  let src =
    value_exn
      ~msg:"Missing 'src' section of config"
      TomlLenses.(
        get
          lookup.Lookup.builds_conf
          (key "src" |-- table))
  in
  TomlTypes.Table.(iter
                     (fun build _ ->
                        if get_should_build lookup (Key.to_string build) then
                          emit_build lookup (Key.to_string build))
                     src)

let test_build_name s = "test-" ^ s

(*
 * Emit a test Makefile.  Like emiting an ocaml makefile, this does some
 * dependency calculation, specifically so th test gets rebuilt if one of the
 * things it depends on changes.
 *)
let emit_test_makefile lookup test =
  let src_dir = path_concat [".."; ".."; ".."; tests_dir; test] in
  let src_path = Filename.concat tests_dir test in
  let files = Sys.readdir src_path |> Array.to_list |> List.filter ~f:(endswith ".ml") in
  let deps = get_test_deps lookup test in
  let internal_deps = Build_set.inter deps (get_builds ()) in
  let external_targets =
    calculate_external_targets
      (path_concat [".."; ".."; lookup.Lookup.build_type])
      lookup.Lookup.builds_conf
      internal_deps
  in
  let extra_compiler_opts =
    value_opt
      ~default:""
      (Lookup.string ~test:true lookup test "extra_compiler_opts")
  in
  let extra_makefile_lines =
    value_opt
      ~default:[]
      (Lookup.strings ~test:true lookup test "extra_makefile_lines")
  in
  let oc =
    open_out
      (path_concat ["build"; test_build_name lookup.Lookup.build_type; test; "Makefile"])
  in
  output_string
    oc
    (String.concat
       "\n\n"
       [ "SRC_DIR=" ^ src_dir
       ; "PACKAGES=" ^ (String.concat " " (Build_set.elements deps))
       ; "TEST_MODULES=" ^ (String.concat " " files)
       ; "EXTERNAL_DEPS=" ^ (String.concat "\\\n\t" (File_set.elements external_targets))
       ; "EXTRA_COMPILER_OPTS=" ^ extra_compiler_opts
       ]);
  output_string oc "\n\n";
  output_string
    oc
    (value_exn
       ~msg:"Could not load test makefile template"
       (Pds_template.read test_makefile));
  output_string
    oc
    (String.concat
       "\n\n"
       ([""] @
        extra_makefile_lines @
        [ "include ../../../Ocamlrules.mk.in"
        ; "$(native_cmx) $(byte_cmo) $(neutral_cmi): ../../../pds.conf"]));
  output_char oc '\n';
  close_out oc

let emit_tests lookup tests =
  Build_set.iter
    (fun test ->
       if get_should_build ~test:true lookup test then begin
         mkdir_p ["build"; test_build_name lookup.Lookup.build_type; test];
         emit_test_makefile lookup test
       end)
    tests

let emit_pds_mk_deps oc lookup builds =
  Build_set.iter
    (fun b ->
       let deps = get_build_deps lookup b in
       let internal_deps = Build_set.inter deps (get_builds ()) in
       let all_deps =
         Build_set.union
           internal_deps
           (get_compile_build_deps lookup b)
       in
       match Build_set.elements all_deps with
         | [] ->
           ()
         | deps ->
           fprintf
             oc
             "%s_%s: %s\n\n"
             lookup.Lookup.build_type
             b
             (String.concat " " (List.map ~f:(sprintf "%s_%s" lookup.Lookup.build_type) deps)))
    builds

let emit_pds_mk_test_deps oc lookup tests =
  Build_set.iter
    (fun t ->
       let deps = get_test_deps lookup t in
       let internal_deps = Build_set.inter deps (get_builds ()) in
       match Build_set.elements internal_deps with
         | [] ->
           ()
         | deps ->
           fprintf
             oc
             "test-%s_%s: %s\n\n"
             lookup.Lookup.build_type
             t
             (String.concat " " (List.map ~f:(sprintf "%s_%s" lookup.Lookup.build_type) deps))
    )
    tests

(*
 * pds.mk orchestrate running tests and building.  This contains all of the
 * dependency information between builds, which order they should be built in,
 * etc.
 *)
let emit_pds_mk builds_conf selector =
  let builds = get_builds () in
  let tests = get_tests () in
  let oc = open_out pds_mk in
  fprintf oc "all:\n\n";
  fprintf oc "PROJECTS = %s\n\n" (String.concat " " (Build_set.elements builds));
  fprintf oc "TESTS = %s\n\n" (String.concat " " (Build_set.elements tests));
  List.iter
    ~f:(fun build_type ->
        let lookup = Lookup.({ build_type; selector; builds_conf }) in
        emit_pds_mk_deps oc lookup builds;
        emit_pds_mk_test_deps oc lookup tests)
    ["release"; "debug"; "profile"];
  output_string oc (value_exn ~msg:"Could not load pds.mk template" (Pds_template.read pds_mk));
  output_char oc '\n';
  fprintf oc "test: $(tests_release)\n\n";
  close_out oc

let emit_ocamlrules_mk () =
  let oc = open_out ocamlrules_mk in
  output_string
    oc
    (value_exn ~msg:"Could not load Ocamlrules.mk" (Pds_template.read ocamlrules_mk));
  close_out oc

let assert_builds_in_builds_conf builds builds_conf =
  Build_set.iter
    (fun b ->
       ignore
         (value_exn
            ~msg:(sprintf "Missing build %s in config" b)
            TomlLenses.(get builds_conf (key "src" |-- table |-- key b |-- table))))
    builds

let emit_makefiles pds_conf =
  let builds_conf = load_builds_conf pds_conf in
  let selector = compute_selector builds_conf in
  let builds = get_builds () in
  let tests = get_tests () in
  assert_builds_in_builds_conf builds builds_conf;
  List.iter
    ~f:(fun build_type ->
        let lookup = Lookup.({ build_type; selector; builds_conf }) in
        emit_builds lookup;
        emit_tests lookup tests)
    ["release"; "debug"; "profile"];
  emit_pds_mk builds_conf selector;
  emit_ocamlrules_mk ()

let emit_formatted_builds builds_conf selector =
  let builds = get_builds () in
  Build_set.iter
    (fun b ->
       let project_target_type_str =
         match get_project_type builds_conf b with
           | `Ocaml ->
             string_of_project_target_type (get_project_target_type builds_conf b)
           | `Third_party ->
             ""
       in
       let lookup = Lookup.({ build_type = "release"; selector; builds_conf }) in
       if get_should_build lookup b then
         print_endline
           (String.concat
              "\t"
              [ "src"
              ; b
              ; project_target_type_str
              ; string_of_project_type (get_project_type builds_conf b)
              ; string_of_deps (Build_set.elements (get_build_deps lookup b))
              ]))
    builds

let emit_formatted_tests builds_conf selector =
  let tests = get_tests () in
  let lookup = Lookup.({ build_type = "release"; selector; builds_conf }) in
  Build_set.iter
    (fun t ->
       if get_should_build ~test:true lookup t then
         print_endline
           (String.concat
              "\t"
              [ "test"
              ; t
              ; string_of_deps (Build_set.elements (get_test_deps lookup t))
              ]))
    tests

let emit_formatted pds_conf =
  emit_makefiles pds_conf;
  let builds_conf = load_builds_conf pds_conf in
  let selector = compute_selector builds_conf in
  emit_formatted_builds builds_conf selector;
  emit_formatted_tests builds_conf selector

let generate_makefiles = function
  | true ->
    (* Format *)
    emit_formatted "pds.conf"
  | false ->
    (* Don't format *)
    emit_makefiles "pds.conf"

let cmd =
  let doc = "Emit build configs" in
  Cmdliner.Term.((const generate_makefiles $ Cmdline.format), info "pds" ~doc)

let main () =
  match Cmdliner.Term.eval cmd with
    | `Error _ -> begin
      Printf.eprintf "Error\n";
      exit 1
    end
    | _ ->
      exit 0

let () = main ()
