.PHONY: clean_pds_template

# Make it serial because we are adding a NON_LIB_MODULE
.NOTPARALLEL:

NON_LIB_MODULES += pds_template.ml

$(SRC_DIR)/pds_template.ml: $(wildcard ../../../files/*)
	ocaml-crunch -m plain ../../../files > $(SRC_DIR)/pds_template.ml

clean: clean_pds_template

clean_pds_template:
	rm $(SRC_DIR)/pds_template.ml
