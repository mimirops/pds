.PHONY: all test clean docs
.PHONY: $(projects_release) $(projects_debug) $(projects_profile)
.PHONY: $(projects_install) $(projects_doc)
.PHONY: $(tests_release) $(tests_debug) $(tests_profile)
.PHONY: $(projects_clean_release) $(projects_clean_debug) $(projects_clean_profile)
.PHONY: $(tests_clean_release) $(tests_clean_debug) $(tests_clean_profile)

projects_release = $(PROJECTS:%=release_%)
projects_debug = $(PROJECTS:%=debug_%)
projects_profile = $(PROJECTS:%=profile_%)
projects_install = $(PROJECTS:%=install_%)
projects_docs = $(PROJECTS:%=docs_%)
projects_remove = $(PROJECTS:%=remove_%)
projects_clean_release = $(PROJECTS:%=clean-release_%)
projects_clean_debug = $(PROJECTS:%=clean-debug_%)
projects_clean_profile = $(PROJECTS:%=clean-profile_%)

tests_release = $(TESTS:%=test-release_%)
tests_debug = $(TESTS:%=test-debug_%)
tests_profile = $(TESTS:%=test-profile_%)
tests_clean_release = $(TESTS:%=test-clean-release_%)
tests_clean_debug = $(TESTS:%=test-clean-debug_%)
tests_clean_profile = $(TESTS:%=test-clean-profile_%)

all: release

release: $(projects_release)

debug: $(projects_debug)

profile: $(projects_profile)

install: $(projects_install)

test: $(tests_release)

test-debug: $(tests_debug)

test-profile: $(tests_profile)

docs: $(projects_docs)

remove: $(projects_remove)

clean:
	-rm -rf build/docs

clean: $(projects_clean_release) $(projects_clean_debug) $(projects_clean_profile)

clean: $(tests_clean_release) $(tests_clean_debug) $(tests_clean_profile)

$(projects_release):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/release:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/release/$(patsubst release_%,%,$@) \
	-C build/release/$(patsubst release_%,%,$@)

$(projects_debug):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/debug:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/debug/$(patsubst debug_%,%,$@) \
	-C build/debug/$(patsubst debug_%,%,$@) debug

$(projects_profile):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/profile:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/profile/$(patsubst profile_%,%,$@) \
	-C build/profile/$(patsubst profile_%,%,$@) profile

$(projects_install): $(projects_release)
	$(MAKE) -C build/release/$(patsubst install_%,%,$@) install

$(tests_release):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/release:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/test-release/$(patsubst profile_%,%,$@) \
	-C build/test-release/$(patsubst test-release_%,%,$@)

$(tests_debug):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/debug:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/test-debug/$(patsubst profile_%,%,$@) \
	-C build/test-debug/$(patsubst test-debug_%,%,$@)

$(tests_profile):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/profile:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/test-profile/$(patsubst profile_%,%,$@) \
	-C build/test-profile/$(patsubst test-profile_%,%,$@)

$(projects_docs): $(projects_release)
	mkdir -p $(shell pwd)/build/docs/$(patsubst docs_%,%,$@)
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/release:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/docs/$(patsubst docs_%,%,$@) \
	-C build/release/$(patsubst docs_%,%,$@) docs

$(projects_remove):
	$(MAKE) -C build/release/$(patsubst remove_%,%,$@) remove

$(projects_clean_release):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/release:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/release/$(patsubst clean-release_%,%,$@) \
	-C build/release/$(patsubst clean-release_%,%,$@) clean

$(projects_clean_debug):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/debug:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/debug/$(patsubst clean-debug_%,%,$@) \
	-C build/debug/$(patsubst clean-debug_%,%,$@) clean

$(projects_clean_profile):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/profile:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/profile/$(patsubst clean-profile_%,%,$@) \
	-C build/profile/$(patsubst clean-profile_%,%,$@) clean

$(tests_clean_release):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/release:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/test-release/$(patsubst test-clean-release_%,%,$@) \
	-C build/test-release/$(patsubst test-clean-release_%,%,$@) test-clean

$(tests_clean_debug):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/debug:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/test-debug/$(patsubst test-clean-debug_%,%,$@) \
	-C build/test-debug/$(patsubst test-clean-debug_%,%,$@) test-clean

$(tests_clean_profile):
	$(MAKE) \
	OCAMLPATH=$(shell pwd)/build/profile:$(OCAMLPATH) \
	BUILD_DIR=$(shell pwd)/build/test-profile/$(patsubst test-clean-profile_%,%,$@) \
	-C build/test-profile/$(patsubst test-clean-profile_%,%,$@) test-clean
